
import java.util.Arrays;
import java.util.Scanner;

public class Program {
    private static Scanner scanner = new Scanner(System.in);
    private static final int MIN_NUMBER_IN_ARRAY_OF_RANGE = -25;
    private static final int MAX_NUMBER_IN_ARRAY_OF_RANGE = 51;

    public static void main(String[] args) {
        int arrayLength = giveLength();
        if (giveLength() <= 0) {
            System.out.println("Введен неверный диапазон массива!");
        } else {
            int[] array = new int[arrayLength];
            randomMassive(array, arrayLength);
            System.out.println(Arrays.toString(array));
            System.out.println("Введите значение элемента, индекс которого хотите найти: ");
            int result = Search(array, arrayLength);
            if (result == -1) {
                System.out.println("Искомого значения нет в списке ");
            } else {
                System.out.println("Номер искомого элемента в последовательности - " + result);
            }
        }
    }
    private static int giveLength() {
        System.out.println("Введите желаемую длинну массива: ");
        return scanner.nextInt();
    }

    private static void randomMassive(int[] massive, int massiveLength) {
        for (int i = 0; i < massiveLength; i++) {
            massive[i] = MIN_NUMBER_IN_ARRAY_OF_RANGE + (int) (Math.random() * MAX_NUMBER_IN_ARRAY_OF_RANGE);
        }
    }

    private static int Search(int[] massive, int massiveLength) {
        int number = scanner.nextInt();

        for (int i = 0; i < massiveLength; i++) {
            if (massive[i] == number) {
                return i;
            }
        }
        return -1;
    }
}
